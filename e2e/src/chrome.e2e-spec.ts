import * as webdriver from 'selenium-webdriver';
// import * as chrome from 'selenium-webdriver/chrome';

const driver = new webdriver.Builder().forBrowser('chrome').build();
const By = webdriver.By;

describe('testing selenium-webdriver directly', () => {
  beforeEach(() => {});

  it('home page title is correct', () => {
    driver.get('http://localhost:4200');
    expect(driver.getTitle()).toEqual('Aventador');
  });

  // afterEach(async () => {
  //   // Assert that there are no errors emitted from the browser
  //   const logs = await browser
  //     .manage()
  //     .logs()
  //     .get(logging.Type.BROWSER);
  //   expect(logs).not.toContain(
  //     jasmine.objectContaining({
  //       level: logging.Level.SEVERE
  //     } as logging.Entry)
  //   );
  // });
});
