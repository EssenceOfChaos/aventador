import { AppPage } from './app.po';
import { browser, logging, element, by } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Welcome to aventador!');
  });

  // it('clicking "Log In" takes you to the auth0 login page', () => {
  //   page.navigateTo();
  //   browser.waitForAngular();
  //   element(by.buttonText('Log In')).click();
  //   browser.waitForAngular();
  //   expect(browser.getTitle()).toBe('Sign In with Auth0');
  // });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser
      .manage()
      .logs()
      .get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    );
  });
});
