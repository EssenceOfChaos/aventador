/**
 * Script Name: {ng-features}
 *
 * Generated using  New Relic Synthetics Formatter for Katalon
 *
 * Feel free to explore, or check out the full documentation
 * https://docs.newrelic.com/docs/synthetics/new-relic-synthetics/scripting-monitors/writing-scripted-browsers
 * for details.
 */

/** IMPORTS **/
// import * as webdriver from 'selenium-webdriver';
// import * as chrome from 'selenium-webdriver/chrome';

const webdriver = require('selenium-webdriver');
const driver = new webdriver.Builder().forBrowser('chrome').build();
const By = webdriver.By;
const waitForAndFindElement = webdriver.waitForAndFindElement;

/** CONFIGURATIONS **/

// Theshold for duration of entire script - fails test if script lasts longer than X (in ms)
var ScriptTimeout = 180000;
// Script-wide timeout for all wait and waitAndFind functions (in ms)
var DefaultTimeout = 30000;
// Change to any User Agent you want to use.
// Leave as "default" or empty to use the Synthetics default.
var UserAgent = 'default';

/** HELPER VARIABLES AND FUNCTIONS **/

const assert = require('assert');

// const browser = $browser.manage()
let $browser = driver;
/** BEGINNING OF SCRIPT **/

console.log('Starting synthetics script: {ng-features}');
console.log('Default timeout is set to ' + DefaultTimeout / 1000 + ' seconds');

// Setting User Agent is not then-able, so we do this first (if defined and not default)
if (UserAgent && 0 !== UserAgent.trim().length && UserAgent != 'default') {
  $browser.addHeader('User-Agent', UserAgent);
  console.log('Setting User-Agent to ' + UserAgent);
}

// Get browser capabilities and do nothing with it, so that we start with a then-able command
$browser
  .getCapabilities()
  .then(function() {})
  .then(() => {
    logger.log(1, 'http://localhost:4200/');
    return $browser.get('http://localhost:4200/');
  })
  .then(() => {
    logger.log(2, 'assert page title is Home');
    return $browser.getTitle().then(function(title) {
      assert.equal(
        'Home',
        title,
        'Assertion failed! Page title is not Home.  Page title: ' + title
      );
    });
  })
  .then(() => {
    logger.log(
      3,
      'assert text of xpath=/html/body/app-root/div/h1 includes Welcome to aventador!'
    );
    return $browser
      .waitForAndFindElement(
        By.xpath('/html/body/app-root/div/h1'),
        DefaultTimeout
      )
      .then(function(el) {
        return el.getText().then(function(text) {
          var found = text.includes('Welcome to aventador!');
          assert.equal(
            true,
            found,
            'Assertion failed! Unable to find text Welcome to aventador! in element xpath=/html/body/app-root/div/h1. Text: ' +
              text
          );
        });
      });
  })
  .then(() => {
    logger.log(
      4,
      "clickElement xpath=(.//*[normalize-space(text()) and normalize-space(.)='Log In'])[1]/following::span[1]"
    );
    return $browser
      .waitForAndFindElement(
        By.xpath(
          "(.//*[normalize-space(text()) and normalize-space(.)='Log In'])[1]/following::span[1]"
        ),
        DefaultTimeout
      )
      .then(function(el) {
        el.click();
      });
  })
  .then(() => {
    logger.log(5, 'clickElement id=mat-input-0');
    return $browser
      .waitForAndFindElement(By.id('mat-input-0'), DefaultTimeout)
      .then(function(el) {
        el.click();
      });
  })
  .then(() => {
    logger.log(6, 'type aapl');
    return $browser
      .waitForAndFindElement(By.id('mat-input-0'), DefaultTimeout)
      .then(function(el) {
        el.clear();
        el.sendKeys('aapl');
      });
  })
  .then(() => {
    logger.log(7, 'sendKeys $driver.Key.ENTER');
    return $browser
      .waitForAndFindElement(By.id('mat-input-0'), DefaultTimeout)
      .then(function(el) {
        el.sendKeys($driver.Key.ENTER);
      });
  })
  .then(() => {
    logger.log(8, 'clickElement link=Aventador');
    return $browser
      .waitForAndFindElement(By.linkText('Aventador'), DefaultTimeout)
      .then(function(el) {
        el.click();
      });
  })

  // WARNING: unsupported command captureEntirePageScreenshot. Object= {"command":"captureEntirePageScreenshot","target":"","value":""}

  .then(
    function() {
      logger.end();
      console.log('Browser script execution SUCCEEDED.');
    },
    function(err) {
      logger.end();
      console.log('Browser script execution FAILED.');
      throw err;
    }
  );

//** Export Functions
const logger = (function(timeout = 3000, mode = 'production') {
  var startTime = Date.now(),
    stepStartTime = Date.now(),
    prevMsg = '',
    prevStep = 0;

  if (typeof $util == 'undefined') {
    $util = {
      insights: {
        set: msg => {
          console.log(`dryRun: sending to Insights using ${msg}`);
        }
      }
    };
  }

  function log(thisStep, thisMsg) {
    if (thisStep > prevStep && prevStep != 0) {
      end();
    }

    stepStartTime = Date.now() - startTime;

    if (mode != 'production') {
      stepStartTime = 0;
    }

    console.log(`Step ${thisStep}: ${thisMsg} STARTED at ${stepStartTime}ms.`);

    prevMsg = thisMsg;
    prevStep = thisStep;
  }

  function end() {
    var totalTimeElapsed = Date.now() - startTime;
    var prevStepTimeElapsed = totalTimeElapsed - stepStartTime;

    if (mode != 'production') {
      prevStepTimeElapsed = 0;
      totalTimeElapsed = 0;
    }

    console.log(
      `Step ${prevStep}: ${prevMsg} FINISHED. It took ${prevStepTimeElapsed}ms to complete.`
    );

    $util.insights.set(`Step ${prevStep}: ${prevMsg}`, prevStepTimeElapsed);
    if (timeout > 0 && totalTimeElapsed > timeout) {
      throw new Error(
        'Script timed out. ' +
          totalTimeElapsed +
          'ms is longer than script timeout threshold of ' +
          timeout +
          'ms.'
      );
    }
  }

  return {
    log,
    end
  };
})(ScriptTimeout);
