const express = require("express");
const bodyParser = require("body-parser");
// const mongodb = require("mongodb");
const path = require('path');
// let ObjectID = mongodb.ObjectID;
const mongoose = require('mongoose');
const methodOverride = require('method-override');

const cors = require('cors')
// Use Env configuration
require('dotenv').config()
// Config
const config = require('./server/config');
const app = express();


// Create link to Angular build directory
let distDir = __dirname + "/dist/";
app.use(express.static(distDir));

// CORS options
let corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
}


mongoose.connect(config.MONGO_URI, {
  useNewUrlParser: true
});
const monDb = mongoose.connection;

monDb.on('error', function () {
  console.error('MongoDB Connection Error. Please make sure that', config.MONGO_URI, 'is running.');
});

monDb.once('open', function callback() {
  console.info('Connected to MongoDB:', config.MONGO_URI);
});
//
// Middleware
/*
 |--------------------------------------
 | App
 |--------------------------------------
 */



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(cors(corsOptions));

// Set port
const port = process.env.PORT || '8080';
app.set('port', port);

// Set static path to Angular app in dist
// Don't run in dev
if (process.env.NODE_ENV !== 'dev') {
  app.use('/', express.static(path.join(__dirname, './dist')));
}

/*
 |--------------------------------------
 | Routes
 |--------------------------------------
 */

require('./server/api')(app, config);

// Pass routing to Angular app
// Don't run in dev
if (process.env.NODE_ENV !== 'dev') {
  app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '/dist/index.html'));
  });
}

/*
 |--------------------------------------
 | Server
 |--------------------------------------
 */

app.listen(port, () => console.log(`Server running on localhost:${port}`));

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({
    "error": message
  });
}
