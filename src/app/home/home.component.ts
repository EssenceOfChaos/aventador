import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  pageTitle = 'Home';
  constructor(public auth: AuthService, private title: Title) {}

  ngOnInit() {
    this.title.setTitle(this.pageTitle);
  }
}
