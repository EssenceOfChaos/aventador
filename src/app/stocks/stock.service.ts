import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StockService {
  private pk = environment.IEX_PK;
  private base_url = 'https://cloud.iexapis.com/beta/tops?token=';
  private v1URL = 'https://api.iextrading.com/1.0/stock/STOCK/price';
  private sectorsURL = environment.SECTOR_PERF;
  constructor(private http: HttpClient) {}

  getPrice(symbol) {
    console.log(`Now fetching the price of ${symbol}`);

    return this.http.get(this.v1URL.replace('STOCK', symbol)).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getSectorPerformance() {
    return this.http.get(this.sectorsURL).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }
  // createProduct(product: any): Observable<any> {
  //   const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  //   product.id = null;
  //   return this.http.post<any>(this.productsUrl, product, { headers }).pipe(
  //     tap(data => console.log('createProduct: ' + JSON.stringify(data))),
  //     catchError(this.handleError)
  //   );
  // }

  // deleteProduct(id: number): Observable<{}> {
  //   const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  //   const url = `${this.productsUrl}/${id}`;
  //   return this.http.delete<Product>(url, { headers }).pipe(
  //     tap(data => console.log('deleteProduct: ' + id)),
  //     catchError(this.handleError)
  //   );
  // }

  // updateProduct(product: Product): Observable<Product> {
  //   const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  //   const url = `${this.productsUrl}/${product.id}`;
  //   return this.http.put<Product>(url, product, { headers }).pipe(
  //     tap(() => console.log('updateProduct: ' + product.id)),
  //     // Return the product on an update
  //     map(() => product),
  //     catchError(this.handleError)
  //   );
  // }

  private handleError(err) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }
}
