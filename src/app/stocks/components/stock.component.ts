import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { StockService } from '../stock.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';

/// ...

import { Observable } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { Stocks, mapOfSymbols } from '../stocks';
// import { Timestamp } from 'rxjs/internal/operators/timestamp';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

interface Sector {
  type: string;
  name: string;
  performance: number;
  lastUpdated: Date;
}


// const typeahead = fromEvent(stockInput, 'input').pipe(
//   map((e: KeyboardEvent) => e.target.value),
//   filter(text => text.length > 2),
//   debounceTime(10),
//   distinctUntilChanged(),
//   switchMap(() => ajax('/api/endpoint'))
// );
@Component({
  selector: 'app-stocks',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})

export class StockComponent implements OnInit {



  constructor(
    public stocks: StockService,
    private title: Title,
    private fb: FormBuilder
  ) {}

  stockForm = this.fb.group({
    symbol: ['', Validators.required]
  });

  public stockPrice;
  public sectors;
  public mapOfSymbols = mapOfSymbols;

  pageTitle = 'Stock Market Data';

  userInput = new FormControl();
  options: string[] = Stocks;
  filteredOptions: Observable<string[]>;

  ngOnInit() {
    // log what the user is typing
    this.userInput.valueChanges.subscribe(result => console.log(result));

    this.filteredOptions = this.userInput.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    // set the page title on component init
    this.title.setTitle(this.pageTitle);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option =>
      option.toLowerCase().includes(filterValue)
    );
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.stockForm.value.symbol);
    this.stocks
      .getPrice(this.stockForm.value.symbol)
      .subscribe(data => (this.stockPrice = data));
  }

  getStocks(symbol) {
    this.stocks.getPrice(symbol).subscribe(stocks => console.log(stocks));
  }

  getSectorPerformance() {
    this.stocks.getSectorPerformance().subscribe(data => (this.sectors = data));
  }

}
