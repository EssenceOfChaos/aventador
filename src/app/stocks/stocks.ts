// import { Map } from '../shared/utils/map';

// export default class Stocks extends Map {
//   constructor() {
//     super();
//   }
// }

// NASDAQ Stock Exchange

export const Stocks = [
  'AAL',
  'AAPL',
  'ADBE',
  'ADP',
  'AMZN',
  'EBAY',
  'FB',
  'MSFT',
  'NFLX',
  'NVDA',
  'QCOM',
  'SBUX',
  'TSLA',
  'WDC',
  'Z',
];

//
export const mapOfSymbols = {
  AAL: 'American Airlines Gp',
  AAPL: 'Apple Inc',
  ADBE: 'Adobe Systems Inc',
  ADP: 'Automatic Data Procs',
  AMZN: 'Amazon.com Inc',
  DNKN: 'Dunkin\' Brands Group',
  EBAY: 'Ebay Inc',
  ERIC: 'Ericsson ADR',
  ETFC: 'E*Trade Finl Corp',
  ETSY: 'Etsy Inc',
  FB: 'Facebook Inc',
  FLWS: '1-800-Flowers.com',
  HAS: 'Hasbro Inc',
  JACK: 'Jack IN The Box Inc',
  KHC: 'Kraft Heinz CO',
  LE: 'Lands\'s End Inc',
  LOGI: 'Logitech Int S.A.',
  LOGM: 'Logmein Inc',
  MAR: 'Marriot Int CI A',
  MAT: 'Mattel Inc',
  MSFT: 'Microsoft Corp',
  NFLX: 'Netflix Inc',
  NVDA: 'Nvidia Corp',
  ODP: 'Office Depot',
  PLAY: 'Dave & Buster\'s Ente',
  PLCE: 'Children\'s PLace Inc',
  PYPL: 'Paypal Holdings',
  PZZA: 'Papa John\'s Intl',
  QCOM: 'Qualcomm Inc',
  RCII: 'Rent-A-Center Inc',
  SBUX: 'Starbucks Corp',
  TIVO: 'Tivo Corp',
  TMUS: 'T-Mobile US',
  TRUE: 'Truecar Inc',
  TSLA: 'Tesla Inc',
  TXN: 'Texas Instruments',
  VRA: 'Vera Bradley',
  VRSN: 'Verisign Inc',
  WEN: 'Wendys Company',
  WDC: 'Western Digital Cp',
  Z: 'Zillow Group Cl C',
  ZUMZ: 'Zumiez Inc'
};
