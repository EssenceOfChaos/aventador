import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  pageTitle = 'Aventador';

  constructor(public auth: AuthService, private title: Title) {
    auth.handleAuthentication();
  }

  ngOnInit() {
    if (this.auth.isAuthenticated()) {
      this.auth.renewTokens();
    }
    this.title.setTitle(this.pageTitle);
  }
}
