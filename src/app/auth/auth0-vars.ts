import { environment } from '../../environments/environment';

interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: environment.CLIENT_ID,
  domain: environment.DOMAIN,
  callbackURL: environment.CALLBACK
};
