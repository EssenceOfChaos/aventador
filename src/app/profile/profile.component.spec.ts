import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProfileComponent } from './profile.component';
import { MaterialModule } from '../material/material.module';
import { AuthService } from '../auth/auth.service';

const authServiceStub = {
  // mock getting a profile
  getProfile() {
    const profile = {
      nickname: 'cooldude17',
      email: 'cooldude17@aol.com',
      name: 'cool Bob',
      age: 25,
      admin: false,
      avatar: 'https://i.imgur.com/zX9DeFW.png',
      fav_colors: ['blue', 'grey', 'yellow']
    };
    return profile;
  }
};

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileComponent],
      imports: [MaterialModule, RouterTestingModule],
      providers: [{ provide: AuthService, useValue: authServiceStub }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
