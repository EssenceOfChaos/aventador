import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profile: any;
  pageTitle = 'Profile';

  constructor(public auth: AuthService, private title: Title) {}

  ngOnInit() {
    this.title.setTitle(this.pageTitle);

    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
  }
}
