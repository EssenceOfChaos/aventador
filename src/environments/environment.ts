// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  DOMAIN: 'swiftlabs.auth0.com',
  CLIENT_ID: '6BvkGP9QR1GjL4skgP5HLdqgA65gTsly',
  CALLBACK: 'http://localhost:4200/callback',
  IEX_PK: 'pk_40c6c71966a445cca7038a5445fd54a0',
  SECTOR_PERF: 'https://api.iextrading.com/1.0/stock/market/sector-performance'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
