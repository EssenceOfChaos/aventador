export const environment = {
  production: true,
  DOMAIN: 'swiftlabs.auth0.com',
  CLIENT_ID: '6BvkGP9QR1GjL4skgP5HLdqgA65gTsly',
  CALLBACK: 'http://localhost:4200/callback',
  IEX_PK: 'pk_40c6c71966a445cca7038a5445fd54a0',
  SECTOR_PERF: 'https://api.iextrading.com/1.0/stock/market/sector-performance'
};
