// server/config.js
module.exports = {
  AUTH0_DOMAIN: 'swiftlabs.auth0.com',
  AUTH0_API_AUDIENCE: 'https://api.rsvpevents.com',
  MONGO_URI: process.env.MONGODB_URI || "mongodb://localhost:27017/test"
};
