FROM node:8.14-alpine


WORKDIR /home/app
ADD package.json /home/app
RUN npm cache clean
RUN npm install
ADD . /home/app

EXPOSE 3000

CMD ["npm", "start"]


